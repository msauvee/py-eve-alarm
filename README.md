
Install python3 : https://www.python.org/

install virtualenv
```
py-eve-alarm> py -m pip install --user virtualenv
```

install a virtual env for the project and activate it

```
py-eve-alarm> py -m virtualenv venv
...
py-eve-alarm>.\venv\Scripts\activate
```

