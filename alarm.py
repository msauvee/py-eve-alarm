#!/usr/bin/python

import os, argparse
import numpy
from PIL import Image, ImageGrab
import cv2
import pygame.mixer
import time

ORANGE = [200, 152, 16]
DARK_BLUE = [7, 39, 120]
LIGHT_BLUE = [46, 105, 195]
GREEN = [30, 121, 33]
GREY = [143, 146, 148]


class Radar:

    def __init__(self, alert_sound_file=None, icons_files=[],
                 verbose=False, save_scan=False, scan_folder=None,
                 zone_computation_interval=60, threshold=0.9,
                 factor=1, sound_volume=0.2):
        self.alert_sound_file = alert_sound_file
        if self.alert_sound_file is not None:
            pygame.mixer.init()
            pygame.mixer.music.load(alert_sound_file)
            pygame.mixer.music.set_volume(sound_volume)
        self.verbose = verbose
        self.save_scan = save_scan
        self.scan_folder = scan_folder
        self.factor = factor
        self.threshold = threshold
        self.icons = []
        self.padding = 20
        self.zone_computation_interval = zone_computation_interval
        for icon_file in icons_files:
            self.icons.append(
                cv2.resize(cv2.cvtColor(numpy.array(Image.open(icon_file)), cv2.COLOR_RGB2BGR), (0, 0), fx=self.factor,
                           fy=self.factor))

        self.colors = [ORANGE, DARK_BLUE, LIGHT_BLUE, GREEN, GREY]
        if self.verbose:
            print('Verbone on')
            print(
                f'factor:{self.factor}, threshold={self.threshold}, padding={self.padding} save_scan={self.save_scan} ({scan_folder})')

    def mean_square_error(self, imageA, imageB):
        # the 'Mean Squared Error' between the two images is the
        # sum of the squared difference between the two images;
        # NOTE: the two images must have the same dimension
        err = numpy.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
        err /= float(255 ** 2 * imageA.shape[0] * imageA.shape[1])

        # return the normed MSE, the lower the error, the more "similar"
        # the two images are
        # 1 means totally different, 0 means same.
        return err

    def scan_image(self, image, zone):
        scan_file = ''
        if self.save_scan:
            scan_file = str(zone[0]) + '_' + str(zone[1]) + 'x' + str(zone[2]) + '_' + str(zone[3]) + '.png'
            cv2.imwrite(os.path.join(self.scan_folder, scan_file), image)

        #if self.verbose:
        #    print(f'scan zone: ({zone[0]}, {zone[1]}), ({zone[2]}, {zone[3]}) {scan_file}')

        for icon in self.icons:
            result = cv2.matchTemplate(image, icon, cv2.TM_CCOEFF_NORMED)
            detected = numpy.nonzero(result >= self.threshold)
            if len(detected[0]) > 0:
                x = detected[1][0]
                y = detected[0][0]
                detected_image = image[y:y + icon.shape[1], x:x + icon.shape[0]]
                mse = self.mean_square_error(detected_image, icon)
                print(f'mean square error: {mse}')
                if mse < 0.005:
                    if self.save_scan:
                        scan_file = 'detected_' + str(x) + '_' + str(y) + '.png'
                        cv2.imwrite(os.path.join(self.scan_folder, scan_file), detected_image)
                    return True, x, y, icon.shape[0], icon.shape[1]

        return False, None, None, None, None

    def scan(self, zones):
        #if self.verbose:
        #    print('scan started')
        for zone in zones:
            grab_done = False
            while not grab_done:
                try:
                    capture = ImageGrab.grab(bbox=zone)
                    grab_done = True
                except OSError:
                    # nothing to do just retry ... always
                    pass

            frame = cv2.cvtColor(numpy.array(capture), cv2.COLOR_RGB2BGR)

            found, x, y, w, y = self.scan_image(frame, zone)
            if found:
                return True

        return False

    def aggregate_point_in_zone(self, pt, zone, max_x, max_y):
        # enlarge zone if necessary and ensure its inside screen
        return (max(0, min(zone[0], pt[0] - self.padding)),
                max(0, min(zone[1], pt[1] - self.padding)),
                min(max_x, max(zone[0], pt[0] + self.padding)),
                min(max_y, max(zone[1], pt[1] + self.padding)))

    def find_zones(self):
        # fullscreen capture
        fullscreen = numpy.array(ImageGrab.grab())

        # look for colors
        coordinates = []
        for color in self.colors:
            indices = (abs(fullscreen - color) < 4).all(axis=-1).nonzero()
            coordinates += list(zip(indices[1], indices[0]))

        # build zones
        zones = []
        points_sorted_by_x = sorted(coordinates, key=lambda tup: tup[0])
        for pt in points_sorted_by_x:
            zone, idx = self.find_zone_near_point(pt, zones)
            if zone is None:
                # create a new zone
                zones.append((pt[0] - self.padding, pt[1] - self.padding, pt[0] + self.padding, pt[1] + self.padding))
            else:
                # enlarge zone if necessary and ensure its inside screen
                zones[idx] = self.aggregate_point_in_zone(pt, zone, fullscreen.shape[1], fullscreen.shape[0])

        print(f'Local zone computed: {zones} from {len(coordinates)} points')
        return zones

    def find_zone_near_point(self, pt, zones):
        for idx, zone in enumerate(zones):
            if zone[0] - self.padding <= pt[0] <= zone[2] + self.padding and zone[1] - self.padding <= pt[1] <= zone[3] + self.padding:
                return zone, idx
        return None, -1

    def scan_in_loop(self):
        last_zone_computation = time.time()
        zones = [(30, 500, 150, 1000)]
        alarm_on = False
        # use ctrl+c to stop it
        while True:
            # recompute zones
            # since_last_zone_computation = time.monotonic() - last_zone_computation
            # if  since_last_zone_computation > self.zone_computation_interval or len(zones) == 0:
            #    zones = self.find_zones()
            #    while len(zones) == 0:
            #        zones = self.find_zones()
            #    last_zone_computation = time.monotonic()

            found = self.scan(zones)
            if found and not alarm_on:
                alarm_on = True
                if self.alert_sound_file is not None:
                    pygame.mixer.music.play()
                print('alarm on ... pause for 25 secondes')
                time.sleep(25)

            if not found and alarm_on:
                if self.verbose:
                    print('alarm off')
                alarm_on = False


def main():
    parser = argparse.ArgumentParser(description='Scan for neutral in local and alart if found !')
    parser.add_argument('-v', '--verbose', help='Verbose logs', action='store_true')
    parser.add_argument('-s', '--savescan', help='Save scanned area in files (useful for debug)', action='store_true')
    parser.add_argument('-z', '--zonecomputationinterval', type=int,
                        help='interval in seconds between local zone detection', default=30, action='store',
                        nargs='?', )
    args = parser.parse_args()
    dir_name = os.path.dirname(__file__)
    scan_folder = os.path.join(dir_name, './scans')
    radar = Radar('resources/submarine-diving-alarm-daniel_simon.mp3', ['resources/yellow.png', 'resources/red.png'], args.verbose,
                  args.savescan, scan_folder, args.zonecomputationinterval)
    radar.scan_in_loop()


if __name__ == "__main__":
    main()
