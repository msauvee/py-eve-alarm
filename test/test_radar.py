from unittest import TestCase
from alarm import Radar
import cv2


class TestRadar(TestCase):
    def test_aggregate_point_in_zone(self):
        radar = Radar()
        zone = radar.aggregate_point_in_zone((50,50), (10, 10, 40, 40), 100, 100)
        self.assertEqual(zone[0], 10)
        self.assertEqual(zone[1], 10)
        self.assertEqual(zone[2], 70)
        self.assertEqual(zone[3], 70)

    def test_detect_on_sample(self):
        radar = Radar(icons_files=['../resources/yellow.png'], save_scan=True, scan_folder='./scans')
        image = cv2.imread('./resources/test-yellow-pc.png')
        found, x, y, w, h = radar.scan_image(image, (0, 0, image.shape[1], image.shape[0]))
        self.assertTrue(found, 'yellow icon not detected !!')
        self.assertEqual(x, 68, 'bad abscissa detection')
        self.assertEqual(y, 45, 'bad ordinate detection')
        self.assertEqual(w, 9, 'bad width detection')
        self.assertEqual(h, 9, 'bad height detection')

    def test_not_detection(self):
        radar = Radar(icons_files=['../resources/yellow.png', '../resources/red.png'], save_scan=True, scan_folder='./scans')
        # image = cv2.imread('./resources/30_600x400_1080.png')
        image = cv2.imread('./resources/detected_253_24.png')
        found, x, y, w, h = radar.scan_image(image, (0, 0, image.shape[1], image.shape[0]))
        self.assertFalse(found, 'should not detect !!')
